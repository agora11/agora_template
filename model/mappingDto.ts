/**
 * agoraApi
 * Api du gisement Agora
 *
 * The version of the OpenAPI document: 1.0
 * Contact: ext.adel.mejri@reseau.sncf.fr
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface MappingDto { 
    codeCp?: string;
    idCaimon?: string;
}

