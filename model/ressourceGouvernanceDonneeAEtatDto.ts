/**
 * agoraApi
 * Api du gisement Agora
 *
 * The version of the OpenAPI document: 1.0
 * Contact: ext.adel.mejri@reseau.sncf.fr
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface RessourceGouvernanceDonneeAEtatDto { 
    etat?: RessourceGouvernanceDonneeAEtatDto.EtatEnum;
    description?: string;
    codeRoleFonctionnel?: string;
}
export namespace RessourceGouvernanceDonneeAEtatDto {
    export type EtatEnum = 'ACTIF' | 'ARCHIVE' | 'SUPPRIME';
    export const EtatEnum = {
        Actif: 'ACTIF' as EtatEnum,
        Archive: 'ARCHIVE' as EtatEnum,
        Supprime: 'SUPPRIME' as EtatEnum
    };
}


